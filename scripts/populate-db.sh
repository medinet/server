#!/bin/bash

if [[ -z "$1" ]]; then
  echo "Specify patient's public key as a first argument"
  exit 1
fi
pk=e629d3cb0caff6ea4828e407d767aca6f150ac3e

type jq &> /dev/null

if [ $? -eq 1 ]; then
  echo "Error: Install jq"
  echo "https://stedolan.github.io/jq/download/"
  exit 1
fi

cd "${0%/*}"

add-record () {
  curl -X POST localhost:3100/patients/$pk/medical-records -H 'Content-Type: application/json' -d @-
}

set -e

recId=$(cat <<EOF | add-record | jq -r ._id
{
  "doctorName": "Петренко Петро",
  "notes": ["abc 1", "xyz 1"]
}
EOF
)

curl -F "files=@$PWD/images/1.jpeg" -F "files=@$PWD/images/2.jpeg" localhost:3100/patients/$pk/medical-records/$recId/files

recId=$(cat <<EOF | add-record | jq -r ._id
{
  "doctorName": "Василенко Василь",
  "notes": ["abc 2", "xyz 2"]
}
EOF
)

curl -F "files=@$PWD/images/3.jpeg" -F "files=@$PWD/images/4.jpeg" localhost:3100/patients/$pk/medical-records/$recId/files