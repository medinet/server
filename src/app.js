const express = require('express')
const path = require('path')
const logger = require('morgan')
const bodyParser = require('body-parser')
const { MongoError } = require('mongodb')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: false }))

app.use('/platform', require('./routes/platform'))
app.use('/patients', require('./routes/patients'))
app.use('/storage', require('./routes/storage'))

app.use(errorHandler)

module.exports = app

function errorHandler(err, req, res, next) {
  if (!err) {
    return next()
  }

  if (res.headersSent) {
    return next(err)
  }

  if (err instanceof MongoError && err.code === 11000) {
    return res
      .status(409)
      .json({ error: 'Duplicate key error' })
      .end()
  }

  console.error(err.stack)
  res
    .status(500)
    .json({ error: err + '' })
    .end()
}
