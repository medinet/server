const mongodb = require('mongodb')
const Grid = require('gridfs-stream')

const { getDb, MEDICAL_HISTORY_COLLECTION } = require('./mongo')

exports.insertOrIgnore = async (id, data) => {
  const db = await getDb()
  const rec = await db.collection(MEDICAL_HISTORY_COLLECTION).findOne({ _id: mongodb.ObjectId(id) })
  if (rec) {
    return { id, imported: null }
  }

  const newRecord = {
    publicKey: data.publicKey,
    clinicName: data.clinicName,
    createdAt: new Date(data.createdAt),
    doctorName: data.doctorName,
    notes: data.notes
  }
  await db.collection(MEDICAL_HISTORY_COLLECTION).insertOne(newRecord)
  await Promise.all(data.files.map(file => exports.addFile(data.publicKey, newRecord._id.toString(), file)))
  return { id, imported: newRecord }
}

exports.addFile = async (publicKey, medRecordId, file) => {
  const db = await getDb()
  const gfs = new mongodb.GridFSBucket(db, { bucketName: MEDICAL_HISTORY_COLLECTION })
  const writestream = gfs.openUploadStream(file.filename, {
    contentType: file.contentType,
    metadata: {
      publicKey: publicKey,
      medicalRecord: medRecordId
    }
  })
  const writePromise = new Promise((resolve, reject) => {
    writestream.once('finish', resolve).once('error', reject)
    file.stream.pipe(writestream)
  })
  await writePromise
}
