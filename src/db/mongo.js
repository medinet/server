const { MongoClient } = require('mongodb')

let db

exports.ADDRESS_INDEXES_COLLECTION = 'addressIndexes'
exports.PATIENTS_COLLECTION = 'patients'
exports.MEDICAL_HISTORY_COLLECTION = 'medicalHistory'
exports.TEMP_STORAGE_COLLECTION = 'tempStorage'
exports.getDb = async () => {
  if (db !== undefined) {
    return db
  }

  const client = await MongoClient.connect(
    process.env.MONGODB_URL,
    { useNewUrlParser: true }
  )
  db = client.db('medinet')
  await ensureIndexes(db)

  return db
}

const ensureIndexes = async db => {
  await db.collection(exports.ADDRESS_INDEXES_COLLECTION).createIndex({ mnemonicHash: 1 }, { unique: true })
  await db.collection(exports.PATIENTS_COLLECTION).createIndex({ publicKey: 1 }, { unique: true })
  await db.collection(exports.MEDICAL_HISTORY_COLLECTION).createIndex({ publicKey: 1, createdAt: 1 })
  await db
    .collection(`${exports.MEDICAL_HISTORY_COLLECTION}.files`)
    .createIndex({ 'metadata.publicKey': 1, 'metadata.medicalRecord': 1, uploadDate: 1 })
  await db.collection(exports.TEMP_STORAGE_COLLECTION).createIndex({ code: 1 })
}
