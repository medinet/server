const { getDb, PATIENTS_COLLECTION } = require('./mongo')

/**
 * Creates patient record
 */
exports.add = async data => {
  const db = await getDb()
  await db.collection(PATIENTS_COLLECTION).insertOne(data)
  return data
}

/**
 * Create or update patient
 */
exports.createOrUpdate = async (publicKey, optionalFields = {}) => {
  if (!publicKey) {
    throw new Error('publicKey is empty')
  }
  const db = await getDb()
  const collection = db.collection(PATIENTS_COLLECTION)
  const patient = await collection.findOneAndUpdate(
    { publicKey },
    {
      $set: { publicKey, ...optionalFields, updatedAt: new Date() },
      $setOnInsert: { createdAt: new Date() }
    },
    { returnNewDocument: true, upsert: true, returnOriginal: false }
    )
  return patient.value
}
