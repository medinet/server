const { getDb, TEMP_STORAGE_COLLECTION } = require('./mongo')

/**
 * Stores data and returns random 6-digit code
 */
exports.store = async data => {
  const db = await getDb()
  const code = Math.floor(100000 + Math.random() * 900000)

  const record = { ...data, code }
  await db.collection(TEMP_STORAGE_COLLECTION).insertOne(record)
  return record
}

/**
 * Finds data by code
 */
exports.find = async code => {
  const db = await getDb()
  // TODO: remove data from temp collection
  return await db.collection(TEMP_STORAGE_COLLECTION).findOne({ code })
}
