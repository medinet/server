const ipfsClient = require('ipfs-http-client')

const ipfs = ipfsClient(process.env.IPFS_HOST)

/**
 * Adds objects and returns ipfs hash
 * @param  {Object} data
 * @return {string}
 */
exports.addJson = async data => {
  const results = await ipfs.add(ipfs.types.Buffer.from(JSON.stringify(data, null, 2)))
  return results[0].hash
}

/**
 * @param  {Object} readableStream
 * @return {string}
 */
exports.addFile = async readableStream => {
  const results = await ipfs.add(readableStream)
  return results[0].hash
}

/**
 * Downloads medical record from IPFS
 * @param  {string} hash
 * @return {Object}
 */
exports.getMedicalRecordWithFiles = async hash => {
  console.log('getMedicalRecordWithFiles, hash:', hash)
  const rec = await exports.getJson(hash)
  const files = await Promise.all(
    rec.ipfsFiles.map(async file => ({ ...file, stream: await exports.getFileStream(file.hash) }))
  )
  return {
    ...rec,
    files
  }
}

/**
 * Finds JSON in IPFS by hash and returns a parsed JS object
 * @param  {string} hash
 * @return {Object}
 */
exports.getJson = async hash => {
  const result = await exports.getFile(hash)
  return JSON.parse(result.toString('utf-8'))
}

/**
 * @param  {string} hash
 * @return {Promise<Buffer>}
 */
exports.getFile = hash => ipfs.cat(hash)

/**
 * @param  {string} hash
 * @return {Promise<Buffer>}
 */
exports.getFileStream = hash => ipfs.catReadableStream(hash)
