const router = require('express-promise-router')()
const mongodb = require('mongodb')
const { checkSchema, validationResult } = require('express-validator/check')
const multer = require('multer')
const GridFsStorage = require('multer-gridfs-storage')
const Grid = require('gridfs-stream')

const { getDb, PATIENTS_COLLECTION, MEDICAL_HISTORY_COLLECTION } = require('../db/mongo')
const patientsDb = require('../db/patients')
const storageDb = require('../db/storage')
const medicalHistoryDb = require('../db/medicalHistory')
const ipfs = require('../ipfs')

/**
 * Creates new user with input data or imports user data from the temp storage
 *
 * curl -i -X POST -H 'Content-Type: application/json' -d '{"publicKey": "pk", "firstName": "John", "lastName": "Doe", "dob": "1978-11-20", "sex": "male", "phone": "(032)329-33-44"}' localhost:3100/patients
 * or
 * curl -i -X POST -H 'Content-Type: application/json' -d '{"code": 191486}' localhost:3100/patients
 */
router.post(
  '/',
  checkSchema({
    code: { in: 'body', isNumeric: true, toNumber: true, errorMessage: 'Value must be a number' },
    firstName: { in: 'body', isString: true, optional: true, errorMessage: 'Value must be a string' },
    lastName: { in: 'body', isString: true, optional: true, errorMessage: 'Value must be a string' },
    sex: { in: 'body', isString: true, optional: true, errorMessage: 'Value must be a string' },
    phone: { in: 'body', isString: true, optional: true, errorMessage: 'Value must be a string' },
    dob: { in: 'body', isISO8601: true, optional: true, errorMessage: 'Value must be in the format YYYY-MM-DD' }
  }),
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    const { code, firstName, lastName, dob, sex, phone } = req.body

    const storedData = await storageDb.find(+code)
    if (!storedData) {
      return res.status(404).json({ error: 'Invalid code' })
    }
    const patient = await patientsDb.createOrUpdate(storedData.publicKey, { firstName, lastName, dob, sex, phone })
    const records = await Promise.all(storedData.records.map(ipfs.getMedicalRecordWithFiles))
    await Promise.all(records.map(rec => medicalHistoryDb.insertOrIgnore(rec._id, rec)))
    return res.status(200).json(patient)
  }
)

/**
 * curl localhost:3100/patients
 */
router.get('/', async (req, res) => {
  const db = await getDb()
  return res.json(
    await db
      .collection(PATIENTS_COLLECTION)
      .find()
      .toArray()
  )
})

/**
 * curl -i -X POST -H 'Content-Type: application/json' -d '{"doctorName": "Bob", "notes": ["abc", "xyz"]}' localhost:3100/patients/wef/medical-records
 */
router.post(
  '/:publicKey/medical-records',
  checkSchema({
    doctorName: { in: 'body', isString: true, errorMessage: 'Value must be a string' },
    notes: { in: 'body', optional: true }
  }),
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    const db = await getDb()
    const data = {
      publicKey: req.params.publicKey,
      doctorName: req.body.doctorName,
      clinicName: process.env.CLINIC_NAME,
      notes: req.body.notes,
      createdAt: new Date()
    }
    await db.collection(MEDICAL_HISTORY_COLLECTION).insertOne(data)
    return res.status(201).json(data)
  }
)

/**
 * curl localhost:3100/patients/wef/medical-records
 */
router.get('/:publicKey/medical-records', async (req, res) => {
  const db = await getDb()
  return res.json(
    await db
      .collection(MEDICAL_HISTORY_COLLECTION)
      .find({ publicKey: req.params.publicKey })
      .toArray()
  )
})

const storage = new GridFsStorage({
  db: getDb(),
  file: (req, file) => ({
    bucketName: MEDICAL_HISTORY_COLLECTION,
    metadata: {
      publicKey: req.params.publicKey,
      medicalRecord: req.params.id
    }
  })
})
const upload = multer({ storage: storage })

/**
 * curl -F 'files=@/home/agornostal/Downloads/pics/background_70.jpg' -F 'files=@/home/agornostal/Downloads/pics/background_74.jpg' localhost:3100/patients/wef/medical-records/id/files
 *
 * Returns a list of relative URLs
 */
router.post('/:publicKey/medical-records/:id/files', upload.array('files', 10), async (req, res) => {
  const db = await getDb()
  const { id, publicKey } = req.params
  const rec = await db
    .collection(MEDICAL_HISTORY_COLLECTION)
    .findOne({ _id: mongodb.ObjectID(id), publicKey: publicKey })
  if (!rec) {
    return res.status(404).json({ error: 'Not Found' })
  }
  return res.status(201).json(req.files.map(file => `/patients/${publicKey}/medical-records/${id}/files/${file.id}`))
})

/**
 * Returns a list of relative URLs to files
 */
router.get('/:publicKey/medical-records/:id/files', async (req, res) => {
  const db = await getDb()
  const gfs = Grid(db, mongodb)
  gfs.collection(MEDICAL_HISTORY_COLLECTION)
  const { id, publicKey } = req.params

  const files = await gfs.files
    .find({ 'metadata.publicKey': publicKey, 'metadata.medicalRecord': id })
    .sort({ uploadDate: 1 })
    .toArray()
  return res.json(files.map(file => `/patients/${publicKey}/medical-records/${id}/files/${file._id}`))
})

router.get('/:publicKey/medical-records/:recId/files/:fileId', async (req, res) => {
  const db = await getDb()
  const gfs = Grid(db, mongodb)
  gfs.collection(MEDICAL_HISTORY_COLLECTION)
  const { recId, fileId, publicKey } = req.params

  const file = await gfs.files.findOne({
    _id: mongodb.ObjectID(fileId),
    'metadata.publicKey': publicKey,
    'metadata.medicalRecord': recId
  })

  if (!file) {
    return res.status(404).json({ error: 'Not Found' })
  }

  var readstream = gfs.createReadStream({ filename: file.filename })
  res.set('Content-Type', file.contentType)
  return readstream.pipe(res)
})

/**
 * curl http://localhost:3100/patients/pk/medical-records/5c0420d2c76d8f085e3cb8b7/ipfs-export
 *
 * Exports files and then the medical with link to the files to IPFS
 */
router.get('/:publicKey/medical-records/:id/ipfs-export', async (req, res) => {
  const db = await getDb()
  const { id, publicKey } = req.params

  // get record
  const rec = await db
    .collection(MEDICAL_HISTORY_COLLECTION)
    .findOne({ _id: mongodb.ObjectID(id), publicKey: publicKey })
  if (!rec) {
    return res.status(404).json({ error: 'Not Found' })
  }

  // get files
  const gfs = Grid(db, mongodb)
  gfs.collection(MEDICAL_HISTORY_COLLECTION)
  const files = await gfs.files
    .find({
      'metadata.publicKey': publicKey,
      'metadata.medicalRecord': id
    })
    .toArray()

  const fileIds = files.map(file => file._id)

  // upload files to ipfs
  const ipfsFiles = await Promise.all(
    files.map(async file => {
      const fileHash = await ipfs.addFile(gfs.createReadStream({ filename: file.filename }))
      console.debug(`File ${file.filename} is available in IPFS at ${fileHash}`)
      return {
        filename: file.filename,
        contentType: file.contentType,
        hash: fileHash
      }
    })
  )

  // upload record data to ipfs
  const data = {
    ...rec,
    ipfsFiles
  }
  const ipfsHash = await ipfs.addJson(data)
  console.log(`Record ${id} is available in IPFS at ${ipfsHash}`)

  // generate temp code
  // const storeResp = await storageDb.store({ publicKey, records: [ipfsHash] })

  return res.json({
    ipfsHash,
    data
    // accessCode: storeResp.code
  })
})

module.exports = router
