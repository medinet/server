const router = require('express-promise-router')()
const { checkSchema, validationResult } = require('express-validator/check')
const { getDb, ADDRESS_INDEXES_COLLECTION } = require('../db/mongo')

/**
 * curl localhost:3100/platform/address-indexes/abc
 */
router.get('/address-indexes/:mnemonicHash', async (req, res) => {
  const { mnemonicHash } = req.params

  const db = await getDb()
  const record = await db.collection(ADDRESS_INDEXES_COLLECTION).findOne({ mnemonicHash })
  if (record) {
    return res.json(record)
  }
  const newRecord = { mnemonicHash, index: 0 }
  await db.collection(ADDRESS_INDEXES_COLLECTION).insertOne(newRecord)
  return res.json(newRecord)
})

/**
 * Increments index
 *
 * curl -X PATCH -i localhost:3100/platform/address-indexes/abc
 */
router.patch('/address-indexes/:mnemonicHash', async (req, res) => {
  const { mnemonicHash } = req.params

  const db = await getDb()
  const record = await db.collection(ADDRESS_INDEXES_COLLECTION).findOne({ mnemonicHash })
  if (!record) {
    return res.status(404).json({ error: 'Not Found' })
  }

  await db.collection(ADDRESS_INDEXES_COLLECTION).updateOne({ mnemonicHash }, { $inc: { index: 1 } })
  record.index += 1
  return res.json(record)
})

module.exports = router
