const router = require('express-promise-router')()
const { checkSchema, validationResult } = require('express-validator/check')

const { store } = require('../db/storage')

/**
 * Endpoint for users
 *
 * curl -X POST -H 'Content-Type: application/json' -d '{"publicKey": "pk", "records": ["QmR7M1nXrKSHFm2dh3DDCwFe1MD8MakiLguboz6PtEqpYo"]}' localhost:3100/storage
 */
router.post(
  '/',
  checkSchema({
    publicKey: {
      in: 'body',
      isAlphanumeric: true,
      toString: true,
      errorMessage: 'Only alphanumeric characters are allowed'
    },
    records: { in: 'body' }
  }),
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }

    const { publicKey, records } = req.body
    const data = await store({ publicKey, records })
    return res.status(201).json(data)
  }
)

/**
 * Endpoint for clinics
 *
 * curl -X POST localhost:3100/storage/784927
 */
router.post('/:code', async (req, res) => {
  // get data by code from temp storage
  // save patient's public key to patients collection if user doesn't exist
  // save records to medicalHistory collection
  // delete data from temp storage
})

module.exports = router
